#include <random>
#include <string>

using namespace std;

string randDNA(int s, string b, int n)
{
	
	mt19937 eng1(s);
	string hold;
	
	uniform_int_distribution<int> unifrm(0, b.size()-1);
	
	for (int i=0; i<n; i++)
	{
		int random = unifrm(eng1);
	    hold += b[random];
    }
return hold;
}
